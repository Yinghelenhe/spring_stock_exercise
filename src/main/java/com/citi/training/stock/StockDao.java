package com.citi.training.stock;

import org.springframework.data.repository.CrudRepository;

public interface StockDao extends CrudRepository<Stock, Long> {

}

