package com.citi.training.stock.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stock.Stock;
import com.citi.training.stock.StockDao;


@RestController
@RequestMapping("/stock")
public class StockController {
	
	//o List<Stock> findAll() 
	// Stock findById(@PathVariable long id)
	// void create (@RequestBody Stock stock)
	// @ResponseBody void deleteById(@PathVariable long id) 
	
	private static final Logger LOG = 
			LoggerFactory.getLogger(StockController.class);

    @Autowired
    StockDao stockdao;
	
	@RequestMapping(method=RequestMethod.GET)
    public Iterable<Stock> findAll(){
    	LOG.info("HTTP GET to findAll()");
    	LOG.debug("This is a much more verbose debugging message!");
        return stockdao.findAll();
    }
	
	@RequestMapping(value="/{id}",method =RequestMethod.GET)
	public Stock findById(@PathVariable long id) {
	    	return stockdao.findById(id).get();
	    }
	
	 @RequestMapping(method=RequestMethod.POST)
	 public Stock save (@RequestBody Stock stock) {
	    	stockdao.save(stock);
	    	return stock;
	    }
	 
	 @RequestMapping(value="/{id}",method =RequestMethod.DELETE)
	 public void remove(@PathVariable long id) {
	    	stockdao.deleteById(id);
	    }
	 
	 

}
